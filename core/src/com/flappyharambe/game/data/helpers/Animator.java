package com.flappyharambe.game.data.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by fraayala19 on 3/22/17.
 */

public class Animator {

    Animation animation;
    Texture sheet;
    float delay, delayTime;
    float statetime;
    boolean loop;

    public Animator(Texture sheet, int frameCols, int frameRows, float delay, float speed, boolean loop){
        this.sheet = sheet;
        this.delay = delay;

        TextureRegion[][] tmp = TextureRegion.split(sheet, sheet.getWidth()/frameCols, sheet.getHeight()/frameRows);

        TextureRegion[] frames = new TextureRegion[frameCols*frameRows];

        int index = 0;
        for(int i = 0; i<frameRows; i++){
            for(int z = 0; z<frameCols; z++){

                frames[index] = tmp[i][z];
                index++;
            }
        }

        animation = new Animation(speed, frames);
        this.loop = loop;
    }


    public TextureRegion getCurrentFrame(){
        if(delayTime<delay){
            delayTime+=Gdx.graphics.getDeltaTime();
            return animation.getKeyFrame(0);
        }
        statetime+= Gdx.graphics.getDeltaTime();
        return animation.getKeyFrame(statetime, loop);
    }

    public void reset(){
        statetime = 0;
    }

}
