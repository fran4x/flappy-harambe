package com.flappyharambe.game.data.helpers;

/**
 * Created by Fran on 9/13/2016.
 */
public class Constants {

    //Whether to use debugRenderer overlay for Box2D
    public static final boolean debugRender = false;

    //Pixels per Box2D meter
    public static final float PIXELS_IN_METER = 45f;

    //Impulse of player flap
    public static final int IMPULSE_FLAP = 15;

    //Player speed
    public static final float PLAYER_SPEED = 4f;
}
