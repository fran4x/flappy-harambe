package com.flappyharambe.game.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GLTexture;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;

import com.flappyharambe.game.data.entities.BackgroundEntity;
import com.flappyharambe.game.data.entities.BorderEntity;
import com.flappyharambe.game.data.entities.TexturedBorderEntity;
import com.flappyharambe.game.data.entities.PipeEntity;
import com.flappyharambe.game.data.entities.PlayerEntity;
import com.flappyharambe.game.data.helpers.Animator;
import com.flappyharambe.game.data.helpers.Constants;

import java.util.ArrayList;
import java.util.Random;
import java.util.RandomAccess;


/**
 * Created by Fran on 9/12/2016.
 */
public class GameScreen extends BaseScreen{

    //Created stage for rendering with Scene2D
    private Stage stage;

    //Created World for physics in Box2D
    private World world;

    //Create DebugRenderer for Box2D
    private Box2DDebugRenderer debugRenderer;
    private Matrix4 debugMatrix;

    //Initial camera position
    private Vector3 position;

    //Player character
    private PlayerEntity player;


    //List of borders
    private ArrayList<BorderEntity> borderList;

    //Current border x;
    private float currentBorderX = -8;
    
    private ArrayList<PipeEntity> pipeList;

    private ArrayList<BackgroundEntity> backgroundList;

    private float currentPipeX = 8;

    private Random randomGen;

    private ArrayList<Float> waypoint;

    private int score;

    private Label.LabelStyle labelStyle;

    private BitmapFont font;

    private Label scoreLabel;

    private Group backgroundGroup;

    public GameScreen(Boot boot) {
        super(boot);


        //Created Scene2D stage for displaying things
        stage = new Stage(new FitViewport(640/ Constants.PIXELS_IN_METER, 360/Constants.PIXELS_IN_METER));
        position = new Vector3(stage.getCamera().position);


        //Create World for Box2D physics
        world = new World(new Vector2(0, -10), true);

        //Create debug renderer for Box2D
        debugRenderer = new Box2DDebugRenderer();

        //stage.setDebugAll(true);

        //Creates list for borders
        borderList = new ArrayList<BorderEntity>();

        backgroundList = new ArrayList<BackgroundEntity>();

        pipeList = new ArrayList<PipeEntity>();

        backgroundGroup = new Group();

        randomGen = new Random();

        waypoint = new ArrayList<Float>();

        font = this.boot.getManager().get("size10.ttf", BitmapFont.class);
        font.setUseIntegerPositions(false);
        labelStyle = new Label.LabelStyle();
        labelStyle.font = font;
        scoreLabel = new Label("0", labelStyle);
        scoreLabel.setSize(1f,1f);
        scoreLabel.setFontScale(0.02f);
        //scoreLabel.scaleBy(0.1f);

        stage.addActor(scoreLabel);

        world.setContactListener(new ContactListener() {
            @Override
            public void beginContact(Contact contact) {
                if((contact.getFixtureA().getUserData()=="player"&&contact.getFixtureB().getUserData()=="pipe")||
                        (contact.getFixtureA().getUserData()=="pipe"&&contact.getFixtureB().getUserData()=="player")){
                    player.setAlive(false);

                }
            }

            @Override
            public void endContact(Contact contact) {

            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });

        //Sets stage as input processor for the game
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {

        this.backgroundList.add(new BackgroundEntity(this.boot.getManager().get("jungle_background.jpg", Texture.class),
                 -4f, 0.5f));


        backgroundGroup.addActor(backgroundList.get(0));

        stage.addActor(backgroundGroup);

        Sound[] monkeySounds = {this.boot.getManager().get("monkey0.mp3", Sound.class),
                this.boot.getManager().get("monkey1.mp3", Sound.class),
                this.boot.getManager().get("monkey2.mp3", Sound.class)};

        //Create player
        player = new PlayerEntity(world,
                new Animator(this.boot.getManager().get("harambe_fall_spritesheet.png", Texture.class), 3, 1, 0f, 0.1f, false),
                new Animator(this.boot.getManager().get("harambe_jump_spritesheet.png", Texture.class), 3, 1, 0f, 1f, false),
                new Vector2(2f,2f),
                monkeySounds);

        //add player ot stage
        stage.addActor(player);



        //Adds listener for certain keys
        stage.addListener(new InputListener(){
            //Checks if screen is touched
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                player.flap();
                return true;
            }
        });

        //Reset camera to initial position
        stage.getCamera().position.set(position);
        stage.getCamera().update();
    }

    @Override
    public void render(float delta) {
        if(!player.getAlive()){
            getBoot().setScreen(new GameOverScreen(getBoot(), score));
        }

        //Manages lower border
        manageBorder();

        managePipe();

        manageScore();

       manageBackground();

        //Cleans the screen
        Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //Gdx.app.log("Gamescreen FPS", (1/delta) + "");

        scoreLabel.setZIndex(100);
        scoreLabel.setPosition(player.getX()+0.5f, 3f);
        //Update the stage.
        stage.act();

        //Add and removes borders

        //Update physics in the world
        world.step(delta, 6, 2);

        //Moves camera with harambe
        stage.getCamera().position.set(player.getX()+2, 0, 0);
        stage.getCamera().update();

        //Render the screen.
        stage.draw();

        //Renders DebugRenderer for Box2D
        if(Constants.debugRender){
            debugRenderer.render(world, stage.getCamera().combined);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    private void manageBorder(){
        while(player.getX()+18>currentBorderX) {
            this.borderList.add(new BorderEntity(world, new Vector2(currentBorderX, 5f)));
            this.stage.addActor(this.borderList.get(borderList.size()-1));
            this.borderList.add(new TexturedBorderEntity(world, new Vector2(currentBorderX, -4.0f),this.boot.getManager().get("lower_border.png",Texture.class)));
            this.stage.addActor(this.borderList.get(borderList.size()-1));
            currentBorderX += 6;
        }
        while(borderList.size()>10){
            borderList.get(0).remove();
            borderList.get(0).detach();
            borderList.remove(0);
        }
    }

    private void managePipe(){
        float lowerPipeX = (float)(randomGen.nextInt(400)-600)/100;
        while(player.getX()+18>currentPipeX){
            this.pipeList.add(new PipeEntity(this.world, this.boot.getManager().get("fence.png",Texture.class),
                    new Vector2(currentPipeX, lowerPipeX), 0.5f, 3f));
            this.pipeList.add(new PipeEntity(this.world, this.boot.getManager().get("flipfence.png",Texture.class),
                    new Vector2(currentPipeX, lowerPipeX + 9), 0.5f, 3f));
            this.stage.addActor(pipeList.get(pipeList.size()-1));
            this.stage.addActor(pipeList.get(pipeList.size()-2));
            waypoint.add(currentPipeX);
            currentPipeX+=8;
        }
        while(pipeList.size()>10){
            pipeList.get(0).remove();
            pipeList.get(0).detach();
            pipeList.remove(0);
        }
    }

    private void manageBackground(){
        while(player.getX()+32>backgroundList.get(backgroundList.size()-1).getX() +1024/Constants.PIXELS_IN_METER){
            this.backgroundList.add(new BackgroundEntity(this.boot.getManager().get("jungle_background.jpg", Texture.class),
                    backgroundList.get(backgroundList.size()-1).getX() +1024/Constants.PIXELS_IN_METER, 0.5f));
            backgroundGroup.addActor(this.backgroundList.get(this.backgroundList.size()-1));
        }

        while(backgroundList.size()>50){
            backgroundList.get(0).remove();
            backgroundList.remove(0);
        }
    }

    private void manageScore(){
        while(!waypoint.isEmpty()&&player.getX()+1>waypoint.get(0)){
            waypoint.remove(0);
            score++;
            scoreLabel.setText(score + "");
            Gdx.app.log("Score", score + "");
            this.boot.getManager().get("airhorn.mp3", Sound.class).play();
        }
    }

    @Override
    public void hide() {
        //remove all actors from stage
        stage.clear();

        //Detach player from world
        player.detach();

        //detach border from world
        for(BorderEntity border : this.borderList){
            border.detach();
        }

        for(PipeEntity pipe : this.pipeList){
            pipe.detach();
        }
    }

    @Override
    public void dispose() {
        //Destroy stage object
        stage.dispose();

        //Dispose Box2D world
        world.dispose();

        font.dispose();
    }
}
