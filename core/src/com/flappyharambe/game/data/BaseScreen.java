package com.flappyharambe.game.data;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

/**
 * Created by Fran on 9/12/2016.
 */
public abstract class BaseScreen implements Screen {

    protected Boot boot;

    public BaseScreen(Boot boot) {
        this.boot = boot;
    }

    protected Boot getBoot(){
        return this.boot;
    }

}
