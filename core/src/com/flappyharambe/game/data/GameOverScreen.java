package com.flappyharambe.game.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.flappyharambe.game.data.UI.HarambeShotEntity;
import com.flappyharambe.game.data.UI.QuickScopeEntity;

/**
 * Created by fraayala19 on 3/21/17.
 */

public class GameOverScreen extends BaseScreen {

    private int finalScore;
    private Stage stage;

    private Image fence;

    private Image background;

    private Skin skin;

    private TextButton button;

    private Label deadLabel;

    private Label faultLabel;

    private Label scoreLabel;

    private Label highScoreLabel;

    private Label newHighScoreLabel;

    private Preferences prefs;

    private int highScore;

    private Group text;

    public GameOverScreen(Boot boot, int finalScore){
        super(boot);
        this.finalScore = finalScore;
        this.stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        text = new Group();

        skin = new Skin(Gdx.files.internal("uiskin.json"));

        fence = new Image(this.boot.getManager().get("fence.png", Texture.class));
        background = new Image(this.boot.getManager().get("jungle_background.jpg", Texture.class));
        fence.scaleBy(6f);
        fence.moveBy(225f,-600f);

        button = new TextButton("Retry", skin);
        button.setSize(200f,100f);
        button.setPosition(10,10);

        button.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                getBoot().setScreen(new GameScreen(getBoot()));
                Gdx.app.log("Retry", "pressed");
            }
        });

        deadLabel = new Label("Harambe's dead!", skin);
        deadLabel.setFontScale(1.5f);
        deadLabel.setPosition(30,300);


        faultLabel = new Label("And it's all your fault!", skin);
        faultLabel.setFontScale(1f);
        faultLabel.setPosition(40,270);

        scoreLabel = new Label("Score: "+finalScore, skin);
        scoreLabel.setPosition(500,300);

        newHighScoreLabel = new Label("New High Score!", skin);
        newHighScoreLabel.setPosition(500,260);


        prefs = Gdx.app.getPreferences("Flappy Harambe");

        highScore = prefs.getInteger("highscore");

        manageHighScore();

        highScoreLabel = new Label("High Score: "+highScore, skin);
        highScoreLabel.setPosition(500,280);
    }

    @Override
    public void show() {
        stage.addActor(background);


        text.addActor(deadLabel);
        text.addActor(faultLabel);
        text.addActor(scoreLabel);
        text.addActor(highScoreLabel);
        
        stage.addActor(text);

        stage.addActor(fence);
        stage.addActor(new HarambeShotEntity(this.boot.getManager().get("harambe_shot_spritesheet.png",Texture.class),
                new Vector2(275,90)));
        stage.addActor(new QuickScopeEntity(this.boot.getManager().get("quickscope_spritesheet.png",Texture.class),
                new Vector2(0,0)));

        stage.addActor(button);

        SequenceAction quickScopeSound = new SequenceAction();

        stage.addAction(Actions.sequence(Actions.fadeIn(0.25f), Actions.run(new Runnable() {
            @Override
            public void run() {
                getBoot().getManager().get("quickscope.mp3",Sound.class).play();
            }
        })));
    }

    @Override
    public void render(float delta) {

        stage.act();

        Gdx.gl.glClearColor(0.4f, 0.5f, 0.8f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void manageHighScore(){
        if(finalScore>highScore){
            prefs.putInteger("highscore",finalScore);
            Gdx.app.log("Score","New High Score!");
            text.addActor(newHighScoreLabel);
        }
    }

}
