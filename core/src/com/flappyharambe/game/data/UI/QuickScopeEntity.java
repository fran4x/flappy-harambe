package com.flappyharambe.game.data.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.flappyharambe.game.data.helpers.Animator;

/**
 * Created by fraayala19 on 3/22/17.
 */

public class QuickScopeEntity extends Actor {
    Animator animator;
    Texture sheet;

    public QuickScopeEntity(Texture sheet, Vector2 position) {
        this.sheet = sheet;
        animator = new Animator(sheet, 5, 10, 0, 0.04f, false);
        setSize(640f, 340f);
        setPosition(position.x, position.y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        batch.draw(animator.getCurrentFrame(), getX(), getY(), getWidth(), getHeight());
    }
}