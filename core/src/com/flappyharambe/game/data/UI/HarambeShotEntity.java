package com.flappyharambe.game.data.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.flappyharambe.game.data.helpers.Animator;

/**
 * Created by fraayala19 on 4/25/17.
 */

public class HarambeShotEntity extends Actor {
    Animator animator;
    Texture sheet;

    public HarambeShotEntity(Texture sheet, Vector2 position){
        this.sheet = sheet;
        animator = new Animator(sheet, 6, 1, 1f, 0.01f, false);
        setSize(128, 128);
        setPosition(position.x, position.y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(animator.getCurrentFrame(), getX(), getY(), getWidth(), getHeight());
    }
}
