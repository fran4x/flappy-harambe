package com.flappyharambe.game.data.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import static com.badlogic.gdx.scenes.scene2d.ui.Table.Debug.actor;

/**
 * Created by Fran on 9/26/2016.
 */
public class BorderEntity extends Actor{
    //The Box2D world the border is in
    private World world;

    //Box2D body
    protected Body body;

    //Fixture for body
    protected Fixture fixture;

    public BorderEntity(World world, Vector2 position){
        this.world = world;

        //Create body
        BodyDef def = new BodyDef();
        def.position.set(position);
        def.type = BodyDef.BodyType.StaticBody;
        body = world.createBody(def);

        //Give shape
        PolygonShape box = new PolygonShape(); //creates the shape
        box.setAsBox(3f, 0.5f); //sets size as 6x1m
        fixture = body.createFixture(box, 0);//create fixture with 0 density
        fixture.setUserData("border");
        box.dispose(); //Destroys shape
    }

    public void detach(){
        body.destroyFixture(fixture);
        world.destroyBody(body);
    }
}
