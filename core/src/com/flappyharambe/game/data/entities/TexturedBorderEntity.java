package com.flappyharambe.game.data.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Fran on 10/18/2016.
 */
public class TexturedBorderEntity extends BorderEntity {

    private Texture texture;

    public TexturedBorderEntity(World world, Vector2 position, Texture texture) {
        super(world, position);
        fixture.setUserData("lowerBorder");
        this.texture = texture;
        setHeight(1f);
        setWidth(6f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha){
        setPosition((body.getPosition().x - 0.5f),
                (body.getPosition().y - 0.5f));
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }
}
