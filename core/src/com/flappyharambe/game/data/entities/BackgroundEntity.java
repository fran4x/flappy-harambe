package com.flappyharambe.game.data.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.flappyharambe.game.data.helpers.Constants;

/**
 * Created by fraayala19 on 5/5/17.
 */

public class BackgroundEntity extends Actor {
    private Texture texture;
    private float speed;

    public BackgroundEntity(Texture texture, float xPosition, float speed){
        this.texture = texture;
        this.speed = speed;
        this.setPosition(xPosition,0);
        this.setSize(1024/ Constants.PIXELS_IN_METER, 360/ Constants.PIXELS_IN_METER);
    }

    @Override
    public void act(float delta) {
        this.setX(this.getX()+speed*delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY()-4f, getWidth(), getHeight());
    }
}
