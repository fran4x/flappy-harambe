package com.flappyharambe.game.data.entities;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.flappyharambe.game.data.helpers.Animator;
import com.flappyharambe.game.data.helpers.Constants;

import java.util.Random;

/**
 * Created by Fran on 9/15/2016.
 */
public class PlayerEntity extends Actor{

    private Animator fallAnimation;
    private Animator jumpAnimation;

    //The Box2D world the player is in
    private World world;

    //Box2D body for the player
    private Body body;

    //The fixture for the body
    private Fixture fixture;

    //Whether the player still lives
    private boolean alive;

    private boolean jumping;

    private Animator currentAnim;

    private Sound[] monkeySounds;

    public PlayerEntity(World world, Animator fallAnimation, Animator jumpAnimation, Vector2 position, Sound[] monkeySounds){
        this.world = world;
        this.fallAnimation = fallAnimation;
        this.jumpAnimation = jumpAnimation;

        currentAnim = fallAnimation;

        this.monkeySounds = monkeySounds;

        alive = true;

        //Create player body
        BodyDef def = new BodyDef(); //Creates body definition
        def.position.set(position); //Puts body in initial position
        def.type = BodyDef.BodyType.DynamicBody; //Make the body dynamic
        body = world.createBody(def);

        //Give it a shape
        PolygonShape box = new PolygonShape(); //Create shape
        box.setAsBox(0.5f, 0.5f); //1x1 box
        fixture = body.createFixture(box,3); //Create fixture
        fixture.setUserData("player"); //Set user data
        box.dispose(); //Destroy shape

        //Set size to be big enough to be seen
        setSize(1f, 1f);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //Sync position with Box2D
        setPosition((body.getPosition().x - 0.5f),
                (body.getPosition().y - 0.5f));
        //Draws
        batch.draw(currentAnim.getCurrentFrame(), getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void act(float delta) {
        //If alive, make him move forward
        if(alive){
            //Changes x velocity to player speed but leaves vertical velocity untouched
            body.setLinearVelocity(Constants.PLAYER_SPEED, body.getLinearVelocity().y);
            body.setAngularVelocity(0f);
            body.setTransform(body.getPosition(), 0f);
        }

        if(jumping&&body.getLinearVelocity().y>0){
            jumping = false;
            currentAnim = fallAnimation;
            currentAnim.reset();
        }
    }

    public void flap(){
        //Gets position of body
        Vector2 position = body.getPosition();

        monkeySounds[new Random().nextInt(monkeySounds.length)].play(1f);

        jumping = true;

        currentAnim = jumpAnimation;
        jumpAnimation.reset();

        //Adds impulse to flap
        body.setLinearVelocity(body.getLinearVelocity().x,0);
        body.applyLinearImpulse(0, Constants.IMPULSE_FLAP, position.x, position.y, true);

    }

    public void setAlive(boolean alive){
        this.alive = alive;
    }

    public boolean getAlive(){
        return alive;
    }

    public void detach(){
        body.destroyFixture(fixture);
        world.destroyBody(body);
    }
}
