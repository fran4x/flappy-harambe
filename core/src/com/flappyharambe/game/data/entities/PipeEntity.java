package com.flappyharambe.game.data.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by fraayala19 on 3/14/17.
 */

public class PipeEntity extends Actor{

    private Texture texture;

    private World world;

    private Body body;

    private Fixture fixture;

    private float height, width;


    public PipeEntity(World world, Texture texture, Vector2 position, float width, float height) {
        this.world = world;
        this.texture = texture;
        this.height = height;
        this.width = width;

        BodyDef def = new BodyDef();
        def.position.set(position);
        def.type = BodyDef.BodyType.StaticBody;
        body = world.createBody(def);

        PolygonShape box = new PolygonShape();

        box.setAsBox(this.width, this.height);
        fixture = body.createFixture(box, 3);
        fixture.setUserData("pipe");
        box.dispose();

        setSize(width*2,height*2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        setPosition(body.getPosition().x - this.width,
                body.getPosition().y - this.height);
        batch.draw(texture, getX(),getY(), getWidth(), getHeight());
    }

    public void detach(){
        body.destroyFixture(fixture);
        world.destroyBody(body);
        this.remove();
    }
}
