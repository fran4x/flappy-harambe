package com.flappyharambe.game.data;

import com.badlogic.gdx.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.flappyharambe.game.*;

public class Boot extends Game{

	// The manager we use to load our assets

	private GameScreen gameScreen;

	private AssetManager manager;

	@Override
	public void create() {
		Gdx.graphics.setResizable(false);
		Gdx.graphics.setWindowedMode(640,360);
		Gdx.graphics.setTitle("Flappy Harambe");
		//Create the assetmanager
		manager = new AssetManager();

		FileHandleResolver resolver = new InternalFileHandleResolver();
		manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
		manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

		FreetypeFontLoader.FreeTypeFontLoaderParameter size1Params = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
		size1Params.fontFileName = "comicsans.ttf";
		size1Params.fontParameters.size = 50;
		manager.load("size10.ttf", BitmapFont.class, size1Params);


		//Load in assets
		manager.load("harambe.jpg", Texture.class);
		manager.load("harambe_falling.png", Texture.class);
		manager.load("fence.png", Texture.class);
		manager.load("flipfence.png", Texture.class);
		manager.load("comicsans.fnt", BitmapFont.class);
		manager.load("quickscope_spritesheet.png", Texture.class);
		manager.load("harambe_shot_spritesheet.png", Texture.class);
		manager.load("lower_border.png", Texture.class);
		manager.load("jungle_background.jpg", Texture.class);
		manager.load("harambe_jump_spritesheet.png",Texture.class);
		manager.load("harambe_fall_spritesheet.png", Texture.class);
		manager.load("monkey0.mp3", Sound.class);
		manager.load("monkey1.mp3", Sound.class);
		manager.load("monkey2.mp3", Sound.class);
		manager.load("quickscope.mp3", Sound.class);
		manager.load("airhorn.mp3", Sound.class);
		//Instruct manager to load assets
		manager.finishLoading();

		//Creates main game screen
		this.gameScreen = new GameScreen(this);

		//Sets current screen to main game screen
		setScreen(gameScreen);
	}

	public AssetManager getManager() {
		return manager;
	}
}