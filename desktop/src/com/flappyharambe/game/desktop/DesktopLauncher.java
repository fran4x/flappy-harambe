package com.flappyharambe.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.flappyharambe.game.data.Boot;

public class DesktopLauncher {
	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.addIcon("harambe_falling.png", Files.FileType.Internal);
		config.addIcon("airhorn.png", Files.FileType.Internal);
		new LwjglApplication(new Boot(), config);

	}
}
